import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import moment from 'moment'
import underscore from 'vue-underscore';

Vue.config.productionTip = false

Vue.use(underscore);
Vue.use(moment)

new Vue({
  render: h => h(App),
}).$mount('#app')
